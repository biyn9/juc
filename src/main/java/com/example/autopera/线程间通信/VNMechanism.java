package com.example.autopera.线程间通信;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Code_lemon
 * @version 1.0.0
 * @description TODO 等待/通知机制实现线程间通信
 * @date 2024/10/21 17:40
 * @title VNMechanism
 */
@Slf4j
// 第一步：创建资源类，定义属性和方法
class Share {
    // 初始值
    private int number = 0;

    // +1的方法
    public synchronized void incr() throws InterruptedException {
        //第二步：判断 干活 通知
        // 判断，使用循环而不是判断，避免出现线程虚假唤醒问题
        while (number != 0) {// 判断number值是否是0，如果不是0，等待
            this.wait();
        }
        // 干活：如果number值是0，那么+1
        number++;
        log.info("当前线程：{}，当前number：{}", Thread.currentThread().getName(), number);
        // 通知其他线程
        this.notifyAll();
    }

    // -1的方法
    public synchronized void decr() throws InterruptedException {
        // 判断，使用循环而不是判断，避免出现线程虚假唤醒问题
        while (number != 1) {
            this.wait();
        }
        // 干活
        number--;
        log.info("当前线程：{}，当前number：{}", Thread.currentThread().getName(), number);
        //通知
        this.notifyAll();
    }
}

public class VNMechanism {


    // 第三步：创建多个线程，调用资源类里面的操作方法
    public static void main(String[] args) {
        Share share = new Share();
        // 创建线程
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    share.incr();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }, "AA").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    share.decr();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }, "BB").start();

    }

}

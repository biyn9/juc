package com.example.autopera.线程间通信;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Code_lemon
 * @version 1.0.0
 * @description TODO 等待/通知机制实现线程间通信
 * @date 2024年10月21日 22:49:43
 * @title VNMechanism
 */
@Slf4j
// 第一步：创建资源类，定义属性和方法
class ShareByLock {
    // 初始值
    private int number = 0;
    // 创建锁
    private Lock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();
    // +1的方法
    public void incr() throws InterruptedException {
        // 上锁
        lock.lock();
        // 干活
        try{
            // 判断，使用循环而不是判断，避免出现线程虚假唤醒问题
            while (number != 0){
                condition.await();
            }
            // 干活
            number++;
            log.info("当前线程：{}，当前number：{}", Thread.currentThread().getName(), number);
            // 通知
            condition.signalAll();
        }finally {
            // 解锁
            lock.unlock();
        }


    }

    // -1的方法
    public void decr() throws InterruptedException {
        // 上锁
        lock.lock();
        // 干活
        try{
            // 判断，使用循环而不是判断，避免出现线程虚假唤醒问题
            while (number != 1){
                condition.await();
            }
            // 干活
            number--;
            log.info("当前线程：{}，当前number：{}", Thread.currentThread().getName(), number);
            // 通知
            condition.signalAll();
        }finally {
            // 解锁
            lock.unlock();
        }
    }
}

public class VNMechanismByLock {


    // 第三步：创建多个线程，调用资源类里面的操作方法
    public static void main(String[] args) {
        ShareByLock shareByLock = new ShareByLock();
        // 创建线程
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    shareByLock.incr();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }, "AA").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    shareByLock.decr();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }, "BB").start();

        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                try {
                    shareByLock.incr();
                }catch (InterruptedException e){
                    throw new RuntimeException(e);
                }
            }
        },"CC").start();

        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                try {
                    shareByLock.decr();
                }catch (InterruptedException e){
                    throw new RuntimeException(e);
                }
            }
        },"DD").start();

    }

}

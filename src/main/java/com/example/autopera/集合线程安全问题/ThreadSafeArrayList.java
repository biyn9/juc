package com.example.autopera.集合线程安全问题;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Code_lemon
 * @version 1.0.0
 * @description TODO 集合线程安全解决方案
 * @date 2024/10/22 11:07
 * @title ThreadSafeArrayList
 */
public class ThreadSafeArrayList {


    public static void unsafeList() {
        // 创建ArrayList集合
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            new Thread(() -> {
                // 向集合中添加数据
                list.add(UUID.randomUUID().toString().substring(0, 8));
                // 从集合中获取内容
                System.out.println(list);
            }, String.valueOf(i)).start();
        }
    }


    public static void safeVector() {
        // 创建ArrayList集合
        List<String> list = new Vector<>();
        for (int i = 0; i < 30; i++) {
            new Thread(() -> {
                // 向集合中添加数据
                list.add(UUID.randomUUID().toString().substring(0, 8));
                // 从集合中获取内容
                System.out.println(list);
            }, String.valueOf(i)).start();
        }
    }

    public static void safeCollections() {
        // 创建ArrayList集合
        List<String> list = Collections.synchronizedList(new ArrayList<>());
        for (int i = 0; i < 30; i++) {
            new Thread(() -> {
                // 向集合中添加数据
                list.add(UUID.randomUUID().toString().substring(0, 8));
                // 从集合中获取内容
                System.out.println(list);
            }, String.valueOf(i)).start();
        }
    }


    public static void safeCopyOnWriteArrayList() {
        // 创建ArrayList集合
        List<String> list = new CopyOnWriteArrayList();
        for (int i = 0; i < 30; i++) {
            new Thread(() -> {
                // 向集合中添加数据
                list.add(UUID.randomUUID().toString().substring(0, 8));
                // 从集合中获取内容
                System.out.println(list);
            }, String.valueOf(i)).start();
        }
    }


    public static void main(String[] args) {

        // 集合线程不安全
        unsafeList();
        // Vector实现线程安全，使用synchronized关键字来实现
        safeVector();
        // Collections实现线程安全，使用synchronized关键字来实现
        safeCollections();
        // CopyOnWriteArrayList实现线程安全，使用写时复制技术来实现
        safeCopyOnWriteArrayList();
    }


}

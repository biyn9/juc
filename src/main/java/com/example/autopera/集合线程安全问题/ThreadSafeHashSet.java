package com.example.autopera.集合线程安全问题;

import java.util.HashSet;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author Code_lemon
 * @version 1.0.0
 * @description TODO HashSet线程不安全解决方案
 * @date 2024/10/22 14:10
 * @title ThreadSafeHashSet
 */
public class ThreadSafeHashSet {

    public static void unsafeHashSet() {

        // 创建HashSet集合
        HashSet hashSet = new HashSet();
        for (int i = 0; i < 30; i++) {
            new Thread(() -> {
                // 向集合中添加数据
                hashSet.add(UUID.randomUUID().toString().substring(0,8));
                // 从集合中获取内容
                System.out.println(hashSet);
            }, String.valueOf(i)).start();
        }
    }

    public static void safeCopyOnWriteArraySet() {
        // 创建HashSet集合
        CopyOnWriteArraySet hashSet = new CopyOnWriteArraySet<>(new HashSet<>());

        for (int i = 0; i < 30; i++) {
            new Thread(()->{
                // 向集合中添加数据
                hashSet.add(UUID.randomUUID().toString().substring(0,8));

                // 从集合中获取内容
                System.out.println(hashSet);
            }).start();
        }
    }

    public static void main(String[] args) {
        // 不安全线程
//        unsafeHashSet();

        // 安全线程
        safeCopyOnWriteArraySet();
    }

}

package com.example.autopera.集合线程安全问题;

import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Code_lemon
 * @version 1.0.0
 * @description TODO
 * @date 2024/10/22 14:18
 * @title ThreadSafeHashMap
 */
public class ThreadSafeHashMap {

    public static void unsafeHashMap() {
        // 创建HashMap
        HashMap<String, String> hashMap = new HashMap();

        // 多线程调用
        for (int i = 0; i < 30; i++) {
            String key = String.valueOf(i);
            new Thread(() -> {
                // 向集合中添加元素
                hashMap.put(key, UUID.randomUUID().toString().substring(0, 8));

                // 从集合中取数元素
                System.out.println(hashMap);
            }, String.valueOf(i)).start();
        }
    }

    public static void safeConcurrentHashMap() {

        // 创建线程安全的HashMao
        ConcurrentHashMap hashMap = new ConcurrentHashMap<>(new HashMap<>());

        // 多线程调用HashMap
        for (int i = 0; i < 30; i++) {
            // 创建Key值
            String key = String.valueOf(i);

            new Thread(() -> {
                // 向集合中添加元素
                hashMap.put(key, UUID.randomUUID().toString().substring(0, 8));
                // 从集合中取出元素
                System.out.println(hashMap);
            }, String.valueOf(i)).start();
        }
    }

    public static void main(String[] args) {
        // 不安全的HashMap
//        unsafeHashMap();
        // 安全的HashMap
        safeConcurrentHashMap();
    }

}

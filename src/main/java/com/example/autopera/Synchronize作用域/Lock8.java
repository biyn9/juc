package com.example.autopera.Synchronize作用域;

import java.util.concurrent.TimeUnit;

/**
 * @author Code_lemon
 * @version 1.0.0
 * @description TODO
 * @date 2024/10/22 14:57
 * @title Lock8
 */

class Phone {

    // 标准同步发送短信
    public synchronized void sendSMS() {
        System.out.println("========sendSMS");
    }

    // 标准同步等待4秒发送短信
    public synchronized void sendSMSWait4() throws InterruptedException {
        TimeUnit.SECONDS.sleep(4);
        System.out.println("========sendSMS");
    }

    // 静态同步发送短信
    public static synchronized void sendStaticSMS() throws InterruptedException {
        TimeUnit.SECONDS.sleep(4);
        System.out.println("========sendSMS");
    }

    // 标准同步发送短信
    public synchronized void sendMail() {
        System.out.println("========sendMail");
    }
    // 静态同步发送邮件
    public static synchronized void sendStaticMail() {
        System.out.println("========sendMail");
    }

    public void getHello() {
        System.out.println("========getHello");
    }
}




class Check {
    // 标准访问
    public void Lock1() throws InterruptedException {
        Phone phone = new Phone();

        new Thread(phone::sendSMS, "sendSMS").start();
        Thread.sleep(200);
        new Thread(phone::sendMail, "sendMail").start();

    }

    // 停留4秒在短信方法内
    public void Lock2() throws InterruptedException {
        Phone phone = new Phone();

        new Thread(() -> {
            try {
                phone.sendSMSWait4();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }, "sendSMSWait4").start();
        Thread.sleep(200);
        new Thread(phone::sendMail, "sendMail").start();

    }

    // 普通Hello方法与短信，先短信还是先Hello
    public void Lock3() throws InterruptedException {
        Phone phone = new Phone();

        new Thread(() -> {
            try {
                phone.sendSMSWait4();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }, "sendSMSWait4").start();
        Thread.sleep(200);
        new Thread(phone::getHello, "getHello").start();

    }

    // 现在有两部手机，先短信还是先邮件？
    public void Lock4() throws InterruptedException {
        Phone phone = new Phone();
        Phone phone2 = new Phone();
        new Thread(() -> {
            try {
                phone.sendSMSWait4();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }, "sendSMSWait4").start();
        Thread.sleep(200);
        new Thread(phone2::sendMail, "sendMail").start();

    }
    // 两个静态同步方法，1部手机，先打印短信还是邮件
    public static void Lock5() throws InterruptedException {
        Phone phone = new Phone();
        new Thread(() -> {
            try {
                phone.sendStaticSMS();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }, "sendSMSWait4").start();
        Thread.sleep(200);
        new Thread(()->{
            phone.sendStaticMail();
        }, "sendMail").start();

    }
    // 两个静态同步方法，2部手机，先打印短信还是邮件
    public static void Lock6() throws InterruptedException {
        Phone phone = new Phone();
        Phone phone2 = new Phone();
        new Thread(() -> {
            try {
                phone.sendStaticSMS();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }, "sendSMSWait4").start();
        Thread.sleep(200);
        new Thread(()->{
            phone2.sendStaticMail();
        }, "sendMail").start();

    }

    // 1个静态同步方法，1个普通同步方法，1部手机，先打印短信还是邮件
    public static void Lock7() throws InterruptedException {
        Phone phone = new Phone();

        new Thread(() -> {
            try {
                phone.sendStaticSMS();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }, "sendSMSWait4").start();
        Thread.sleep(200);
        new Thread(()->{
            phone.sendMail();
        }, "sendMail").start();

    }

    // 1个静态同步方法，1个普通同步方法，2部手机，先打印短信还是邮件
    public static void Lock8() throws InterruptedException {
        Phone phone = new Phone();
        Phone phone1 = new Phone();
        new Thread(() -> {
            try {
                phone.sendStaticSMS();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }, "sendSMSWait4").start();
        Thread.sleep(200);
        new Thread(()->{
            phone1.sendMail();
        }, "sendMail").start();

    }
}

public class Lock8 {

    /**
     * 1、标准访问
     * ========sendSMS
     * ========sendMail
     * 2、停留4秒在短信方法内
     * ========sendSMS
     * ========sendMail
     * 3、普通Hello方法与短信，先短信还是先Hello
     * ========getHello
     * ========sendSMS
     * 4、现在有两部手机，先短信还是先邮件？
     * ========sendMail
     * ========sendSMS
     * 5、两个静态同步方法，1部手机，先打印短信还是邮件
     * ========sendSMS
     * ========sendMail
     * 6、两个静态同步方法，2部手机，先打印短信还是邮件
     * ========sendSMS
     * ========sendMail
     * 7、1个静态同步方法，1个普通同步方法，1部手机，先打印短信还是邮件
     * ========sendMail
     * ========sendSMS
     * 8、1个静态同步方法，1个普通同步方法，2部手机，先打印短信还是邮件
     * ========sendMail
     * ========sendSMS
     */



    public static void main(String[] args) throws InterruptedException {
        Check check = new Check();
//        check.Lock1();
//        check.Lock2();
//        check.Lock3();
//        check.Lock4();
//        Check.Lock5();
//        Check.Lock6();
//        Check.Lock7();
//        Check.Lock8();
    }

}

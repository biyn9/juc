package com.example.autopera.辅助类演示;

import java.util.concurrent.CountDownLatch;

/**
 * @author Code_lemon
 * @version 1.0.0
 * @description TODO CountDownLatch辅助类演示
 * @date 2024/10/22 20:00
 * @title CountDownLatchDemo
 */
public class CountDownLatchDemo {

    public static void main(String[] args) throws InterruptedException {

        // 创建CountDownLatch对象，设置初始值
        CountDownLatch countDownLatch = new CountDownLatch(5);

        for (int i = 0; i < 5; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName() + "号同学离开教室");
                // 计数-1
                countDownLatch.countDown();
            }, String.valueOf(i) ).start();
        }
        // 等待
        countDownLatch.await();


        System.out.println(Thread.currentThread().getName() +"班长锁门走人");
    }

}

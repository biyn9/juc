package com.example.autopera.辅助类演示;

import java.util.concurrent.CyclicBarrier;

/**
 * @author Code_lemon
 * @version 1.0.0
 * @description TODO 循环栅栏辅助类演示
 * @date 2024/10/22 20:13
 * @title CyclicBarrierDemo
 */
public class CyclicBarrierDemo {


    private static final int NUMBER = 6;

    public static void main(String[] args) {

        CyclicBarrier cyclicBarrier = new CyclicBarrier(NUMBER, () -> {
            System.out.println("******集齐7颗龙珠，可以召唤神龙******");
        });

        for (int i = 1; i <= 7; i++) {
            new Thread(() -> {
                try{
                    System.out.println(Thread.currentThread().getName() + "星龙珠被收集到了");

                    // 等待
                    cyclicBarrier.await();
                }catch (Exception e){
                    e.toString();
                }
            }, String.valueOf(i)).start();
        }
    }

}

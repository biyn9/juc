package com.example.autopera.定制化线程通信;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Code_lemon
 * @version 1.0.0
 * @description TODO 定制化线程间通信
 * @date 2024年10月22日 09:50:21
 * @title VNMechanismByLock
 */


@Slf4j
// 第一步：创建资源类，定义属性和方法
class ShareResource {

    // 定义标志位
    private int flag = 1;// AA 1    BB 2    CC 3

    // 创建lock锁
    private Lock lock = new ReentrantLock();

    // 创建三个condition
    private Condition c1 = lock.newCondition();
    private Condition c2 = lock.newCondition();
    private Condition c3 = lock.newCondition();


    // 打印5次，参数为轮数

    /**
     * @param loop 轮数
     */
    public void print5(int loop) throws InterruptedException {
        // 上锁
        lock.lock();
        // 干活
        try {
            // 判断
            while (flag != 1) {
                c1.await();
            }
            // 干活
            for (int i = 1; i <= 5; i++) {
                log.info("当前线程：{}当前数值{}当前轮数{}", Thread.currentThread().getName(), i, loop);
            }
            // 通知
            flag = 2;// 修改标志位 BB
            c2.signal();// 通知BB线程
        } finally {
            // 解锁
            lock.unlock();
        }
    }

    /**
     * TODO 打印10次
     *
     * @param loop 循环次数
     */
    public void print10(int loop) throws InterruptedException {
        // 上锁
        lock.lock();
        // 干活
        try {
            // 判断
            while (flag != 2) {
                c2.await();
            }
            // 干活
            for (int i = 1; i <= 10; i++) {
                log.info("当前线程：{}当前数值{}当前轮数{}", Thread.currentThread().getName(), i, loop);
            }
            // 通知
            flag = 3;// 修改标志位 CC
            c3.signal();// 通知CC线程
        } finally {
            // 解锁
            lock.unlock();
        }
    }

    public void print15(int loop) throws InterruptedException {
        // 上锁
        lock.lock();
        // 干活
        try {
            // 判断
            while (flag != 3) {
                c3.await();
            }
            // 干活
            for (int i = 1; i <= 15; i++) {
                log.info("当前线程：{}当前数值{}当前轮数{}", Thread.currentThread().getName(), i, loop);
            }

            // 通知
            flag = 1;// 修改标志位 AA
            c1.signal();// 通知AA线程
        } finally {
            // 解锁
            lock.unlock();
        }


    }

}

public class VNMechanismByLock {


    public static void main(String[] args) {
        // 创建资源实例
        ShareResource shareResource = new ShareResource();

        new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                try {
                    shareResource.print5(i);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }, "AA").start();

        new Thread(() -> {
            try {
                for (int i = 1; i <= 10; i++) {
                    shareResource.print10(i);
                }

            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }, "BB").start();

        new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                try {
                    shareResource.print15(i);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }, "CC").start();
    }

}

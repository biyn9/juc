package com.example.autopera.线程池;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Code_lemon
 * @version 1.0.0
 * @description TODO
 * @date 2024/10/23 14:38
 * @title CachedThreadPoolExample
 */
public class CachedThreadPoolExample {

    public static void main(String[] args) {
        // 一池可扩容线程池
        ExecutorService threadPool = Executors.newCachedThreadPool();

        try {
            for (int i = 1; i <= 200; i++) {
                threadPool.execute(() -> {
                    System.out.println(Thread.currentThread().getName() + "正在处理业务");
                });
            }
        }catch (Exception e){

        }finally {
            // 关闭线程池
            threadPool.shutdown();
        }

    }
}

package com.example.autopera.线程池;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author Code_lemon
 * @version 1.0.0
 * @description TODO
 * @date 2024/10/23 14:29
 * @title SingleThreadExecutorExample
 */
public class SingleThreadExecutorExample {
    public static void main(String[] args) {

        // 一池一线程
        ExecutorService threadPool = Executors.newSingleThreadExecutor();
        // 干活
        try {
            for (int i = 1; i <= 5; i++) {
                final int taskId = i;
                threadPool.execute(() -> {
                    System.out.println(Thread.currentThread().getName() + "正在处理第" + taskId + "份日志文件");
                    try {
                        TimeUnit.SECONDS.sleep(new Random().nextInt(3));
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                });
            }
        } catch (Exception e) {

        } finally {
            // 关闭线程池
            threadPool.shutdown();
        }
    }

}

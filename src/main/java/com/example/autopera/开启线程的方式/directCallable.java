package com.example.autopera.开启线程的方式;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

/**
 * @author Code_lemon
 * @version 1.0.0
 * @description TODO Callable捷酷实现
 * @date 2024/10/22 16:31
 * @title directCallable
 */
public class directCallable {


    public static void main(String[] args) throws ExecutionException, InterruptedException {

        FutureTask futureTask = new FutureTask(()->{
            System.out.println(Thread.currentThread().getName() + "进入Callable接口");
            TimeUnit.SECONDS.sleep(2);
            return 2014;
        });

        new Thread(futureTask, "AA").start();

        while (futureTask.isDone()){
            System.out.println("等待中.....");
        }

        // 调用FutureTask的Get方法
        System.out.println(futureTask.get());

        System.out.println(futureTask.get());

        System.out.println(Thread.currentThread().getName() + "线程执行结束");

    }

}

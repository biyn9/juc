package com.example.autopera.开启线程的方式;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Code_lemon
 * @version 1.0.0
 * @description TODO 直接使用Runnable接口方式实现
 * @date 2024/10/21 11:14
 * @title directRunnable
 */
@Slf4j
public class directRunnable {

    public static void main(String[] args) {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                log.debug("running");
            }
        };
        Thread t = new Thread(r, "t2");
        t.start();

        log.info("start");
    }
}

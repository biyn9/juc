package com.example.autopera.开启线程的方式;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author Code_lemon
 * @version 1.0.0
 * @description TODO 直接使用Thread内部类方式实现
 * @date 2024/10/21 11:14
 * @title directThread
 */
@Slf4j
public class directFutureTask {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTask<Integer> task = new FutureTask<>(() -> {
            log.info("FutureTask");
            return 100;

        });

        new Thread(task, "t3").start();

        Integer result = task.get();


    }
}

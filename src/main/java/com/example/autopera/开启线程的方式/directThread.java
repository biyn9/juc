package com.example.autopera.开启线程的方式;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Code_lemon
 * @version 1.0.0
 * @description TODO 直接使用Thread内部类方式实现
 * @date 2024/10/21 11:14
 * @title directThread
 */
@Slf4j
public class directThread {

    public static void main(String[] args) {
        // 构造方法的参数是给线程指定名字，推荐
        Thread t1 = new Thread("t1") {
            @Override
            // run 方法内实现了要执行的任务
            public void run() {
                log.info("running");
            }
        };
        t1.start();
        log.info("start");
    }
}

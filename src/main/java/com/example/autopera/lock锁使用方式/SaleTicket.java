package com.example.autopera.lock锁使用方式;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Code_lemon
 * @version 1.0.0
 * @description TODO
 * @date 2024/10/21 16:58
 * @title SaleTicket
 */
@Slf4j
// 第一步：创建资源类，在资源类中创建属性和操作方法
class Ticket {

    // 票数上限
    private int number = 300;
    //创建重入锁
    private final ReentrantLock lock = new ReentrantLock();

    // 买票方法
    public void sale() {
        // 第一步：上锁
        lock.lock();
        // 第二步：判断票数
        try {
            if (number > 0) {
                log.info("当前线程{}：,当前票数：{},剩余票数：{}", Thread.currentThread().getName(), number--, number);
            }
        } catch (Exception e) {
            log.error(e.toString());
        } finally {
            // 第三步：解锁
            lock.unlock();
        }


    }
}

public class SaleTicket {

    // 第二步：创建多个线程，调用资源类的操作方法
    public static void main(String[] args) {
        // 创建ticket实例
        Ticket ticket = new Ticket();
        // 创建三个线程

        new Thread(()->{
            for (int i = 0; i < 400; i++) {
                ticket.sale();
            }
        },"AA").start();
        // 使用lambda表达式
        new Thread(() -> {
            for (int i = 0; i < 400; i++) {
                ticket.sale();
            }
        }, "BB").start();
        // 使用lambda表达式
        new Thread(() -> {
            for (int i = 0; i < 400; i++) {
                ticket.sale();
            }
        }, "CC").start();
    }


}

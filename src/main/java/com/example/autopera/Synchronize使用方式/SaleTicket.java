package com.example.autopera.Synchronize使用方式;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Code_lemon
 * @version 1.0.0
 * @description TODO
 * @date 2024/10/21 16:21
 * @title SaleTicket
 */
@Slf4j
// 第一步：创建资源类，在资源类创建属性和操作方法
class Ticket{
    // 票数上限
    private int number = 300;

    public synchronized void sale(){
        // 判断是否有票
        if(number > 0){
            log.info("当前线程{}：,当前票数：{},剩余票数：{}", Thread.currentThread().getName(), number--, number);
        }
    }

}
@Slf4j
public class SaleTicket {
    // 第二步：创建多个线程，调用资源类的操作方法
    public static void main(String[] args) {
        // 创建ticket实例
        Ticket ticket = new Ticket();

        // 创建三个线程
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 400; i++) {
                    ticket.sale();
                }
            }
        }, "AA").start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 400; i++) {
                    ticket.sale();
                }
            }
        },"BB").start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 400; i++) {
                    ticket.sale();
                }
            }
        },"CC").start();
    }

}
